# 午休假条管理软件 noonsleep
## 开发背景
> 以下内容描述的是开始开发时的情况。现在由于“双减”政策，学校要求全体学生按时睡午觉，并把社团活动纳入课表。因此午休假条已成为历史。
> 
> 然而此软件将继续被维护。

我校大部分同学住校，学校要求午饭后集体回宿舍午休。很多社团活动、少先队/共青团的活动、各学生部门的活动、演出排练都在午休时间进行，这就需要午休假条。一个午休假条大概是这样的：

> <p align=center>请假条</p>
> 尊敬的 X 老师（生活老师）：
> 
> <p style="text-indent:2em">我因为参加 XXX 活动无法回寝休息，望批准。</p>
> <p align=right>请假人：XXX<br>班主任签字：XXX<br>-德育处盖章-</p>

请假的同学需要班主任的签字和德育处的盖章，还需要请一位要睡午觉的同学帮忙把假条带给生活老师才可以安全地在中午参加活动。

生活老师每天中午会清点出没到且没请假的同学，并在wx群里通告。

九月月末的时候，学校组织大家给校长提意见。当时就有了这样的一个想法：

|时间|班主任(class)|生活老师(dorm)|德育处(admin)|
|---|---|---|---|
|午休时间前|上传请假休息到服务器||上传请假休息到服务器|
|午休时间||上传考勤信息到服务器||
|紧随其后|收到 邮件/钉钉/企业wx/app消息 请假情况|||
|清除数据前|查看请假情况|查看请假情况|查看请假情况|

提给学校后，同学转达给我的答复大致是：“实现数字化需要很长时间。”当时我：“？？？”，然后说：“我觉得我一个月就能做一个出来。”（啊啊啊啊啊啊啊我是傻逼）

……于是就有了这个东西。这个仓库放的是后端代码。前端在[这里](https://gitee.com/nep-0/noonsleep_app/)。

这个东西前端用`Flutter`，后端用`Go`来实现。目前来看代码质量不高、功能不完善。我刚刚才学`Flutter`（应该说是为了这个东西才学的），写得乱七八糟的只是勉强能用。

对不起，`README.md`写得也是乱七八糟。

## 使用方法
[**移动端** 操作演示视频](https://www.bilibili.com/video/BV1Dq4y1G7ut)
1. 编译
```
go build
```

2. 运行
- Linux

```
./noonsleep
```

- Windows

```
.\noonsleep
```

- MacOS

    没用过，不知道哈哈。

## 贡献
可恶，这个项目太垃圾，都不好意思让别人知道。怎么会有人贡献呢？
1. Issue
2. Pull Request

## 开源协议
```
MIT License

Copyright (c) 2021 Jeff

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
