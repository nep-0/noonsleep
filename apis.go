package main

import (
	"crypto/md5"
	"fmt"
	"io"
	"sort"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	_ "github.com/mattn/go-sqlite3"
)

type User struct {
	Id        int64
	Name      string `xorm:"unique"`
	Character string // "dorm", "class" or "admin"
	Password  string
	Token     string
}

type Student struct {
	Id           int64
	Name         string
	ClassTeacher int64 // class teacher's ID
	DormTeacher  int64 // dorm teacher's ID
}

type Event struct {
	Id      int64
	Time    time.Time `xorm:"created"`
	User    int64
	Student int64
	Type    string // "application" or "absence"
	Date    string // ISO 8601 Eg."1949-10-01"
}

type StudentStatus struct {
	StudentName string
	Status      string
	ApplierName string
	AbsenceName string
}

func MD5(str string) string {
	w := md5.New()
	io.WriteString(w, str)
	md5str := fmt.Sprintf("%x", w.Sum(nil))
	return md5str
}

func login(c *gin.Context) {
	name := c.PostForm("name")
	password := c.PostForm("password")
	user := User{Name: name}
	has, _ := engine.Get(&user)

	// empty username
	if name == "" {
		c.String(403, "Forbidden.")
		return
	}

	// user not exist
	if !has {
		c.String(403, "Forbidden.")
		return
	}

	// wrong password
	if user.Password != MD5(password) {
		c.String(403, "Forbidden.")
		return
	}

	token := uuid.New().String()

	c.SetCookie("token", token, 3600, "/", "", false, true)
	user.Token = token
	engine.Where("id = " + strconv.FormatInt(user.Id, 10)).Update(&user)

	c.String(200, token)
}

func logout(c *gin.Context) {
	token, err := c.Cookie("token")

	// no token in cookie
	if err != nil {
		token = c.PostForm("token")
		if token == "" {
			c.String(200, "OK.")
			return
		}
	}

	// delete cookie
	c.SetCookie("token", "token", -1, "/", "", false, true)

	var user User
	has, _ := engine.Where("token = ?", token).Get(&user)

	// no such user
	if !has {
		c.String(200, "OK.")
		return
	}

	// delete token in DB
	user.Token = "token"
	engine.Where("id = " + strconv.FormatInt(user.Id, 10)).Update(&user)

	c.String(200, "OK.")
}

func getUser(c *gin.Context) *User {
	token, err := c.Cookie("token")

	// no token in cookie
	if err != nil {
		token = c.PostForm("token")
		if token == "" {
			return nil
		}
	}

	if token == "token" {
		return nil
	}

	user := User{Token: token}
	has, _ := engine.Get(&user)

	// user not exist
	if !has {
		return nil
	}

	return &user
}

func getStudents(c *gin.Context) {
	user := getUser(c)
	if user == nil {
		c.String(401, "Unauthorized.")
		return
	}

	students := make([]*Student, 0)
	student := Student{}

	// see their own students only
	if user.Character == "dorm" {
		student.DormTeacher = user.Id
	} else if user.Character == "class" {
		student.ClassTeacher = user.Id
	}

	engine.Find(&students, student)
	c.JSON(200, students)
}

func apply(c *gin.Context) {
	user := getUser(c)

	// an applier should be either a "class" or an "admin"
	if user == nil {
		c.String(401, "Unauthorized.")
		return
	}
	if user.Character == "dorm" {
		c.String(401, "Unauthorized.")
		return
	}

	studentID, _ := strconv.ParseInt(c.PostForm("stu"), 10, 64)
	date := c.PostForm("date")

	student := Student{Id: studentID}
	has, _ := engine.Get(&student)

	// no such student
	if !has {
		c.String(400, "Bad Request.")
		return
	}
	// not his/her student
	if user.Character != "admin" && student.ClassTeacher != user.Id {
		c.String(401, "Unauthorized.")
		return
	}

	dateObj, err := time.Parse("2006-01-02", date)

	// wrong format of date
	if err != nil {
		c.String(400, "Bad Request.")
		return
	}
	// date in the past
	if dateObj.Add(24 * time.Hour).Before(time.Now()) {
		c.String(400, "Bad Request.")
		return
	}

	event := Event{
		Student: studentID,
		Date:    date,
		Type:    "application",
	}
	has, _ = engine.Get(&event)

	// prevent dup
	if has {
		c.String(201, "Created.")
		return
	}

	event.User = user.Id
	engine.Insert(&event)

	c.String(201, "Created.")
}

func absent(c *gin.Context) {

	// should be either a "dorm" or an "admin"
	user := getUser(c)
	if user == nil {
		c.String(401, "Unauthorized.")
		return
	}
	if user.Character == "class" {
		c.String(401, "Unauthorized.")
		return
	}

	studentID, _ := strconv.ParseInt(c.PostForm("stu"), 10, 64)
	date := c.PostForm("date")

	student := Student{Id: studentID}
	has, _ := engine.Get(&student)

	// no such student
	if !has {
		c.String(400, "Bad Request.")
		return
	}
	// not his/her student
	if user.Character != "admin" && student.DormTeacher != user.Id {
		c.String(401, "Unauthorized.")
		return
	}

	dateObj, err := time.Parse("2006-01-02", date)

	// wrong format of date
	if err != nil {
		c.String(400, "Bad Request.")
		return
	}
	// date in the past
	if dateObj.Add(24 * time.Hour).Before(time.Now()) {
		c.String(400, "Bad Request.")
		return
	}

	event := Event{
		Student: studentID,
		Date:    date,
		Type:    "absence",
	}

	// prevent dup
	has, _ = engine.Get(&event)
	if has {
		c.String(201, "Created.")
		return
	}

	event.User = user.Id
	engine.Insert(&event)

	c.String(201, "Created.")
}

func addStudent(c *gin.Context) {
	user := getUser(c)

	// must be an "admin"
	if user == nil {
		c.String(401, "Unauthorized.")
		return
	}
	if user.Character == "dorm" {
		c.String(401, "Unauthorized.")
		return
	}
	if user.Character == "class" {
		c.String(401, "Unauthorized.")
		return
	}

	name := c.PostForm("name")
	classTeacher, _ := strconv.ParseInt(c.PostForm("classTeacher"), 10, 64)
	dormTeacher, _ := strconv.ParseInt(c.PostForm("dormTeacher"), 10, 64)

	// check if the "dorm" exist
	check := User{Id: classTeacher}
	has, _ := engine.Get(&check)
	if !has {
		c.String(400, "Bad Request.")
		return
	}
	if check.Character == "dorm" {
		c.String(400, "Bad Request.")
		return
	}

	// check if the "class" exist
	check = User{Id: dormTeacher}
	has, _ = engine.Get(&check)
	if !has {
		c.String(400, "Bad Request.")
		return
	}
	if check.Character != "dorm" {
		c.String(400, "Bad Request.")
		return
	}

	student := Student{
		Name:         name,
		ClassTeacher: classTeacher,
		DormTeacher:  dormTeacher,
	}

	// prevent dup
	has, _ = engine.Get(&student)
	if has {
		c.String(201, "Created.")
		return
	}

	engine.Insert(student)
	c.String(201, "Created.")
}

func addUser(c *gin.Context) {
	user := getUser(c)

	// must be an "admin"
	if user == nil {
		c.String(401, "Unauthorized.")
		return
	}
	if user.Character == "dorm" {
		c.String(401, "Unauthorized.")
		return
	}
	if user.Character == "class" {
		c.String(401, "Unauthorized.")
		return
	}

	name := c.PostForm("name")
	character := c.PostForm("character")
	password := c.PostForm("password")

	// empty username
	if name == "" {
		c.String(400, "Bad Request.")
		return
	}

	// empty password
	if password == "" {
		c.String(400, "Bad Request.")
		return
	}

	// invalid character
	if character != "admin" && character != "dorm" && character != "class" {
		c.String(400, "Bad Request.")
		return
	}

	// unique username
	check := User{Name: name}
	has, _ := engine.Get(&check)
	if has {
		c.String(403, "Forbidden.")
		return
	}

	addUser := User{
		Name:      name,
		Character: character,
		Password:  MD5(password),
		Token:     "token",
	}

	engine.Insert(addUser)
	c.String(201, "Created.")
}

func changePassword(c *gin.Context) {
	token, err := c.Cookie("token")

	// no token in cookie
	if err != nil {
		token = c.PostForm("token")
		if token == "" {
			c.String(400, "Bad Request.")
			return
		}
	}

	if token == "token" {
		c.String(400, "Bad Request.")
		return
	}

	user := User{Token: token}
	has, _ := engine.Get(&user)

	// user not exist
	if !has {
		c.String(400, "Bad Request.")
		return
	}

	oldPassword := c.PostForm("old")
	newPassword := c.PostForm("new")

	if oldPassword == "" {
		c.String(400, "Bad Request.")
		return
	}

	if newPassword == "" {
		c.String(400, "Bad Request.")
		return
	}

	// old password not correct
	if MD5(oldPassword) != user.Password {
		c.String(401, "Unauthorized.")
		return
	}

	user.Password = MD5(newPassword)
	engine.Where("id = " + strconv.FormatInt(user.Id, 10)).Update(&user)

	c.String(201, "Created.")
}

func getEvent(c *gin.Context) []*Event {
	user := getUser(c)
	if user == nil {
		c.String(401, "Unauthorized.")
		return nil
	}

	date := c.PostForm("date")

	// wrong format of time
	_, err := time.Parse("2006-01-02", date)
	if err != nil {
		c.String(400, "Bad Request.")
		return nil
	}

	events := make([]*Event, 0)
	event := Event{Date: date}
	engine.Find(&events, event)

	// see their own students only
	var checkStudent Student
	validEvents := make([]*Event, 0)
	if user.Character == "dorm" {
		for _, v := range events {
			checkStudent = Student{Id: v.Student}
			engine.Get(&checkStudent)
			if checkStudent.DormTeacher == user.Id {
				validEvents = append(validEvents, v)
			}
		}
	} else if user.Character == "class" {
		for _, v := range events {
			checkStudent = Student{Id: v.Student}
			engine.Get(&checkStudent)
			if checkStudent.ClassTeacher == user.Id {
				validEvents = append(validEvents, v)
			}
		}
	} else {
		// an "admin" sees all students
		validEvents = events
	}
	return validEvents
}

func getEventDate(c *gin.Context) {
	validEvents := getEvent(c)
	if validEvents == nil {
		return
	}
	c.JSON(200, validEvents)
}

func getStatusDate(c *gin.Context) {
	validEvents := getEvent(c)
	if validEvents == nil {
		return
	}

	// status: "applied"
	//      or "absent"
	//      or "invalidly applied"
	studentStatus := make(map[int64]*StudentStatus)
	var has bool
	var user User
	var student Student

	for _, event := range validEvents {
		user = User{
			Id: event.User,
		}
		engine.Get(&user)

		_, has = studentStatus[event.Student]
		if !has {
			student = Student{
				Id: event.Student,
			}
			engine.Get(&student)

			switch event.Type {
			case "application":
				studentStatus[event.Student] = &StudentStatus{
					Status:      "invalidly applied",
					ApplierName: user.Name,
					StudentName: student.Name,
				}
			case "absence":
				studentStatus[event.Student] = &StudentStatus{
					Status:      "absent",
					AbsenceName: user.Name,
					StudentName: student.Name,
				}
			}
		} else {
			studentStatus[event.Student].Status = "applied"
			switch event.Type {
			case "application":
				studentStatus[event.Student].ApplierName = user.Name
			case "absence":
				studentStatus[event.Student].AbsenceName = user.Name
			}
		}
	}

	statusList := make([]*StudentStatus, 0)
	for _, v := range studentStatus {
		statusList = append(statusList, v)
	}

	sort.Slice(statusList, func(i, j int) bool {
		return statusList[i].Status > statusList[j].Status
	})

	c.JSON(200, statusList)
}
