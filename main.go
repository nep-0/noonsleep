package main

import (
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	_ "github.com/mattn/go-sqlite3"
	"xorm.io/xorm"
)

var r = gin.Default()
var engine, _ = xorm.NewEngine("sqlite3", "data.db")

func main() {
	r.Use(cors.Default()) // allow all origins

	time.LoadLocation("Local")

	engine.Sync(new(User), new(Student), new(Event))

	r.GET("/api", func(c *gin.Context) {
		c.String(200, "Hello.")
	})

	r.POST("/api/login", login)
	r.POST("/api/logout", logout)

	r.POST("/api/getStudents", getStudents)
	r.POST("/api/getEventDate", getEventDate)
	r.POST("/api/getStatusDate", getStatusDate)

	r.POST("/api/apply", apply)
	r.POST("/api/absent", absent)

	r.POST("/api/addStudent", addStudent)
	r.POST("api/addUser", addUser)
	r.POST("api/changePassword", changePassword)

	r.Run()
}
